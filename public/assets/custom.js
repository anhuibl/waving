/**
 * 
 * CUSTOMIZED JAVASCRIPT
 * Functions, Ajax,...
 * Author: anhuibl
 * 
 */

/**
 * useModal - Load data to a modal with id: #emptyModal
 * @param {text} title - To set modal title
 * @param {text} url - To processor
 * @returns {html data}
 * Author: anhuibl.dn
 */
function useModal(title, url) {
    $('#emptyModal .modal-title').empty().html(title);  //Append title to .modal-title
    var body = $('#emptyModal .modal-body');
    $.post(url).done(function (data) {
        body.empty().html(data);                        //Append data to .modal-body
    }).error(function () {
        body.empty().html('Server error!');             //Append data to .modal-body
        setTimeout(function () {
            $('.close').click();                        //Hide modal
        }, 2000);
    });
}

/**
 * msgBox
 * @param {string} data - message to insert into box
 * @param {string} type - danger/success/warning
 * @param {boolean} autoClose - true/false
 * @returns html - alertDataMessageWithType
 */
function msgBox(data, type, autoClose) {
    //uses: #messageBox
    var box = $("#messageBox");
    box.empty();
    box.html('<div class="alert alert-block alert-' + type + '">'
            + data + '<i class="close"></i></div>');
    $('.close').click(function () {
        box.children("div").fadeOut(function () {
            box.empty();
        });
    });
    if (autoClose === true) {
        setTimeout(function () {
            $('#messageBox .close').click();
        }, 5000);
    }
}
//Clear message box content
function msgBoxInit() {
    //uses: #messageBox
    $("#messageBox").empty();
}

/**
 * ajaxForm - perform an ajax request form
 * @param {string} formId - id of form
 * @param {string} loadingSelector - loading selector (element selector: class, id, name...)
 * @returns {json} data - process & append json data with 
 *  success status (data.success {boolean}), errors (data.error {array})
 *  or message (data.msg {string}) into html document.
 */
function ajaxForm(formId, loadingSelector) {
    //uses: msgBoxInit(), msgBox(), .help-block, input name & .help- with input name
    $("#" + formId).submit(function (e) {
        e.preventDefault();
        msgBoxInit();
        $(loadingSelector).fadeIn();
        var $form = $(this),
                url = $form.attr('action'),
                data = $form.serialize();
        var posting = $.post(url, {formData: data});
        posting.done(function (data) {
            var errors = $('#' + formId + ' .help-block');
            errors.empty();
            if (data.success) {
                msgBox(data.msg, "success", true);
                //$('html, body').animate({scrollTop: $('#messageBox').offset()}, 300).stop();
            } else {
                if (data.msg) {
                    msgBox(data.msg, "danger");
                    //ScrollTo("#messageBox");
                } else {
                    var i = 0;
                    $.each(data.errors, function (index, value) {
                        i++;
                        if (i === 1) {
                            $('input[name=' + index + ']').focus();
                        }
                        $('.help-' + index).html(value);
                    });
                }
            }
            $(loadingSelector).fadeOut();
        });
    });
}

/**
 * deleteRecord - Delete any record of any model
 * @param {text} url - Deleting url: /delete/{Model}/{id}
 * @returns true/false (success/fail)
 * Author: anhuibl.dn
 */
function deleteRecord(url) {
    if (confirm('Are you sure?')) {
        $.post(url).done(function (data) {
            if (data === '1')
                location.reload();
            else
                alert('Server error!');
        }).error(function () {
            alert('Server error!');
        });
    }
}


function loadMore() {
    $('.loadMore').click(function (e) {
        e.preventDefault();
        var btn = $(this);
        var url = btn.attr('data-url'),
                key = btn.attr('data-key'),
                page = btn.attr('data-page'),
                total_page = btn.attr('data-total-page');
        if (parseInt(page) + 1 == total_page) {
            btn.fadeOut();
        }
        $.post(url, {id: key, current: page})
                .done(function (data) {
                    if (data.success) {
                        $('.appendTo').append(data.html);
                        btn.attr('data-page', parseInt(page) + 1);
                    }
                });
    });
}

/**
 * 
 * CUSTOMIZED JQUERY EVENTS
 * Author: anhuibl.dn
 */

$(document).ready(function () {
    //Initialize the change profile & password event
    ajaxForm("changeProfile", '.task-state');
    ajaxForm("changePassword", '.task-state');
    ajaxForm("changeSettings", '.task-state');
    //ajaxForm("changeAvatar", '.task-state');
    loadMore();

    $('#changeAvatar>input[name=avatar]').change(function () {
        $("#changeAvatar").submit();
    });

    //Change user image (avatar/cover) - width validation
    //Author: anhuibl
    /*
     $('#changeAvatar > input[name=avatar]').change(function () {          //Detect the input change event
     var minHeight = 768, minWidth = 768, maxSize = 1024;            //Set minimum image size (height/width - pixel, size - KB)
     var imgName = $(this).attr('name');
     if (imgName === 'cover')
     minWidth = 1366;
     //Start read and check file input:
     var file = this.files;
     if (file && file[0]) {                                          //When file input is not empty:
     var reader = new FileReader(), image = new Image();         //Create file and image object
     reader.readAsDataURL(file[0]);                              //Read file URL
     reader.onload = function (_tmp) {                           //When file is loaded:
     image.src = _tmp.target.result;
     image.onload = function () {                            //When image is loaded:
     var size = ~~(file[0].size / 1024);
     var w = this.width, h = this.height;                //Get image attribute (pixel)
     if (w >= minWidth &&
     h >= minHeight &&
     size <= maxSize) {                          //Check minimum
     $('#changeAvatar').submit();
     } else {
     $('#changeAvatar').children('input').val('');
     alert('The ' + imgName + ' must be:\n   - Max size:' + maxSize + ' KB\n   - Min height: '
     + minHeight + 'px\n   - Min width: ' + minWidth + 'px');
     }
     image.onerror = function () {                       //When orror occured:
     alert('Invalid file: ' + file[0].type);
     };
     };
     };
     }
     //End reading file
     });
     */
});