<?php

class Category extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Table relationship declearations:
     */
    public function childrens(){
        return $this->hasMany("Category", "parent_id");
    }

    public function user() {
        return $this->belongsTo("User");
    }

    public function products() {
        return $this->hasMany("Product");
    }

    /**
     * validator - User validator for input request
     * @param array $input - form input
     * @param string $type - name of request
     * @return validator
     */
    public function validator($input) {
        $rules = array(
            'firstname' => 'required|max:100',
            'lastname' => 'required|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required|between:6, 32|confirmed',
        );
        return Validator::make($input, $rules);
    }

}
