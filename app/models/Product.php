<?php

class Product extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Table relationship declearations:
     */
    public function user() {
        return $this->belongsTo("User");
    }

    public function category() {
        return $this->belongsTo("Category");
    }

    /**
     * validator - User validator for input request
     * @param array $input - form input
     * @param string $type - name of request
     * @return validator
     */
    public function validator($input) {
        $rules = array(
            'name' => 'required|max:255',
            'description' => 'required',
            'details' => "required",
            'price' => "required|numeric",
            'promotion' => "numeric",
            'size' => 'max:100',
            'color' => 'max:100',
            'branch' => 'max:100',
            'meta_title' => 'max:100',
            'meta_keywords' => 'max:1000',
            'meta_description' => 'max:255',
        );
        return Validator::make($input, $rules);
    }

}
