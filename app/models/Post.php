<?php

class Post extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * Table relationship declearations:
     */
    public function user() {
        return $this->belongsTo("User");
    }

    public function category() {
        return $this->belongsTo("PostCategory", 'post_category_id');
    }

    /**
     * validator - User validator for input request
     * @param array $input - form input
     * @param string $type - name of request
     * @return validator
     */
    public function validator($input) {
        $rules = array(
            'name' => 'required|max:255',
            'description' => 'required',
            'details' => "required",
            'meta_title' => 'max:100',
            'meta_keywords' => 'max:1000',
            'meta_description' => 'max:255',
        );
        return Validator::make($input, $rules);
    }

}
