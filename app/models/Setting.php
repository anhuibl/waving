<?php

class Setting extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * validator - User validator for input request
     * @param array $input - form input
     * @param string $type - name of request
     * @return validator
     */
    public function validator($input) {
        $rules = array(
            'key' => 'required|max:100|unique:config,key',
            'value' => 'required|max:1000',
        );
        return Validator::make($input, $rules);
    }

}
