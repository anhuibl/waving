<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
     * validator - User validator for input request
     * @param array $input - form input
     * @param string $type - name of request
     * @return validator
     */
    public function validator($input, $type = "register") {
        if ($type == "profile") {
            //For change profile
            $user = $this->find(Auth::user()->id);
            $email = "required|email|unique:users,email";
            $phone = 'digits_between: 9, 11|unique:users,phone';
            if ($input['email'] == $user->email) {
                $email = "";
            }
            if ($input['phone'] == $user->phone) {
                $phone = "";
            }
            $rules = array(
                'firstname' => 'required|max:100',
                'lastname' => 'required|max:100',
                'email' => $email,
                'phone' => $phone,
                'address' => 'max:100',
            );
        } elseif ($type == "password") {
            //For change password
            $rules = array(
                'password' => 'required|between:6, 32|confirmed',
                'current_password' => 'required',
            );
        } elseif ($type == "login") {
            //For login
            $rules = array(
                'email' => 'required|email',
                'password' => 'required|between:6, 32',
            );
        } else {
            //For register
            $rules = array(
                'firstname' => 'required|max:100',
                'lastname' => 'required|max:100',
                'email' => 'required|email|unique:users',
                'password' => 'required|between:6, 32|confirmed',
            );
        }
        return Validator::make($input, $rules);
    }

}
