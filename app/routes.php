<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
//Route::pattern('id', '[0-9]+');

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('/not-found', array('as' => '404', 'uses' => 'HomeController@notFound'));
Route::get('/test', array('as' => 'test', 'uses' => 'HomeController@test'));
Route::get('/about-us', array('as' => 'aboutUs', 'uses' => 'HomeController@aboutUs'));
Route::get('/contact-us', array('as' => 'contactUs', 'uses' => 'HomeController@contactUs'));
Route::get('/privacy-policy', array('as' => 'privacyPolicy', 'uses' => 'HomeController@privacyPolicy'));
Route::get('/terms-and-conditions', array('as' => 'termsConditions', 'uses' => 'HomeController@termsConditions'));
Route::get('/faq', array('as' => 'faq', 'uses' => 'HomeController@faq'));
Route::post('/search', array('as' => 'search', 'uses' => 'HomeController@search'));

//products
Route::get('/products', array('as' => 'products', 'uses' => 'ProductsController@index'));
Route::get('/products/list:{id}={alias?}', array('as' => 'productsCategory', 'uses' => 'ProductsController@category'));
Route::get('/products/view:{id}={alias?}', array('as' => 'productsShow', 'uses' => 'ProductsController@show'));
Route::get('/sales', array('as' => 'sales', 'uses' => 'ProductsController@sales'));

//carts
Route::get('/shopping-cart', array('as' => 'shoppingCart', 'uses' => 'CartsController@shoppingCart'));
Route::get('/checkout', array('as' => 'checkout', 'uses' => 'CartsController@checkout'));

//posts
Route::get('/news', array('as' => 'news', 'uses' => 'PostsController@index'));
Route::any('/news/list:{id}={alias?}', array('as' => 'newsCategory', 'uses' => 'PostsController@category'));
Route::get('/news/read:{id}={alias?}', array('as' => 'newsShow', 'uses' => 'PostsController@show'));

//users
Route::group(array('before' => 'onlyGuest'), function() {
    Route::controller('password', 'RemindersController');
    Route::any('/login', array('as' => 'login', 'uses' => 'UsersController@login'));
    Route::any('/register', array('as' => 'register', 'uses' => 'UsersController@register'));
});
Route::group(array('before' => 'auth'), function() {
    Route::get('/account', array('as' => 'account', 'uses' => 'UsersController@index'));
    Route::post('/change/{type?}', array('as' => 'change', 'uses' => 'UsersController@update'));
    Route::get('/logout', array('as' => 'logout', 'uses' => 'UsersController@logout'));
    Route::get('/whishlist', array('as' => 'whishlist', 'uses' => 'UsersController@whishlist'));
});
