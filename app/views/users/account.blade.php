@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src='{{asset('assets/global/plugins/zoom/jquery.zoom.min.js')}}' type="text/javascript"></script><!-- product zoom -->
<script src="{{asset('assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
<script src="{{asset('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
    Layout.initImageZoom();
    Layout.initTouchspin();
    Layout.initUniform();

    $('.color-panel li').click(function () {
        $('input[name=theme_color]').val($(this).attr('data-style'));
    });

    $('#changeAvatarBtn').click(function () {
        $('#changeAvatar input[name=avatar]').click();
    });
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="{{route('home')}}">Home</a></li>
    <li class="active">Account</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    @include('layouts.partials.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-7">
        <div class="content-page">

            <h3 class="no-top-space">My Account</h3>
            <div id="messageBox">@if(!empty(Session::get('status')))<div class="alert alert-block alert-success">{{Session::get('status')}}</div>@endif</div>
            <!-- TABS -->
            <div class="tab-style-1">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab" aria-expanded="true">Information</a></li>
                    <li class=""><a href="#tab-2" data-toggle="tab" aria-expanded="false">Change profile</a></li>
                    <li><a href="#tab-3" data-toggle="tab">Change password</a></li>
                    <li><a href="#tab-4" data-toggle="tab">Settings</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane row fade active in" id="tab-1">
                        <div class="col-md-3 col-sm-3">
                            <form id="changeAvatar" action="{{route("change", "avatar")}}" method="post" enctype="multipart/form-data">
                                <a href="{{imageSrc($user->avatar, ['users'])}}" class="fancybox-button" title="{{$user->firstname." ".$user->lastname}}" data-rel="fancybox-button">
                                    <img class="img-responsive" src="{{imageSrc($user->avatar, ['users'])}}" alt="">
                                </a>
                                <label for="avatarInput" class="change-avatar fa fa-camera"></label>
                                <span class="help-block require help-avatar">@if(!empty(Session::get('error'))){{Session::get('error')}}@endif</span>
                                <input name="avatar" type="file" id="avatarInput" class="hidden">
                            </form>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <h4 class="no-top-space">{{$user->firstname." ".$user->lastname}}</h4>
                            <p class="text-muted">
                                {{$user->email}}<br/>
                                <strong>{{$user->phone}}</strong>
                                @if(!empty($user->address))<br/>
                                <em>{{$user->address}}</em>
                                @endif
                            </p>
                            <p class="text-muted">Last visited at {{$user->updated_at->format('Y/m/d H:i')}}</p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-2">
                        <form id="changeProfile" role="form" class="form-horizontal form-without-legend" action="{{route('change', "")}}" method="post">
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="first-name">First Name <span class="require">*</span></label>
                                <div class="col-lg-8">
                                    <input name="firstname" value="{{$user->firstname}}" type="text" id="first-name" class="form-control">
                                    <span class="help-block help-firstname require"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="last-name">Last Name <span class="require">*</span></label>
                                <div class="col-lg-8">
                                    <input name="lastname" value="{{$user->lastname}}" type="text" id="first-name" class="form-control">
                                    <span class="help-block help-lastname require"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="email">E-Mail <span class="require">*</span></label>
                                <div class="col-lg-8">
                                    <input name="email" value="{{$user->email}}" type="text" id="email" class="form-control">
                                    <span class="help-block help-email require"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="telephone">Phone</label>
                                <div class="col-lg-8">
                                    <input name="phone" value="{{$user->phone}}" type="text" id="telephone" class="form-control">
                                    <span class="help-block help-phone require"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="address">Address</label>
                                <div class="col-lg-8">
                                    <textarea name="address" class="form-control" id="address">{{$user->address}}</textarea>
                                    <span class="help-block help-address require"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-md-offset-2 padding-top-20">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-default" type="reset">Reset</button>
                                    <span class="task-state pull-right padding-top-10" style="display: none"><img src="{{asset('assets/global/img/input-spinner.gif')}}" alt="Loading" /> Processing...</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="tab-3">
                        <form id="changePassword" role="form" class="form-horizontal form-without-legend" action="{{route('change', 'password')}}" method="post">
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="password">New password <span class="require">*</span></label>
                                <div class="col-lg-7">
                                    <input name="password" type="password" id="password" class="form-control">
                                    <span class="help-block help-password require"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="password_confirmation">Confirm password <span class="require">*</span></label>
                                <div class="col-lg-7">
                                    <input name="password_confirmation" type="password" id="password_confirmation" class="form-control">
                                    <span class="help-block help-password_confirmation require"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="current_password">Current password <span class="require">*</span></label>
                                <div class="col-lg-7">
                                    <input name="current_password" type="password" id="current_password" class="form-control">
                                    <span class="help-block help-current_password require"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-7 col-md-offset-3 padding-top-20">
                                    <button class="btn btn-primary" type="submit">Change</button>
                                    <button class="btn btn-default" type="reset">Clear</button>
                                    <span class="task-state pull-right padding-top-10" style="display: none">
                                        <img src="{{asset('assets/global/img/input-spinner.gif')}}" alt="Loading" /> Processing...
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="tab-4">
                        <form id="changeSettings" role="form" class="form-horizontal form-without-legend" action="{{route('change', "setting")}}" method="post">
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-8 checkbox-list">
                                    <label>
                                        <input name="newsletter" type="checkbox" @if(!empty($newsletter)) checked @endif> Singup for newsletter.
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-8 checkbox-list">
                                    <label>
                                        <input name="fixed_header" type="checkbox" @if($user->fixed_header) checked @endif> Fixed header.
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-8 color-panel" style="z-index: 1; position: relative; top: auto">
                                    <div class="color-mode" style="display: inline-block; position: relative; right: auto; width: auto">
                                        <p>My theme color</p>
                                        <ul class="inline">
                                            <li class="color-red <?php
                                            if ($user->theme_color == 'red'): echo "current";
                                            endif
                                            ?>" data-style="red"></li>
                                            <li class="color-blue <?php
                                            if ($user->theme_color == 'blue'): echo "current";
                                            endif
                                            ?>" data-style="blue"></li>
                                            <li class="color-green <?php
                                            if ($user->theme_color == 'green'): echo "current";
                                            endif
                                            ?>" data-style="green"></li>
                                            <li class="color-orange <?php
                                            if ($user->theme_color == 'orange'): echo "current";
                                            endif
                                            ?>" data-style="orange"></li>
                                            <li class="color-gray <?php
                                            if ($user->theme_color == 'gray'): echo "current";
                                            endif
                                            ?>" data-style="gray"></li>
                                            <li class="color-turquoise <?php
                                            if ($user->theme_color == 'turquoise'): echo "current";
                                            endif
                                            ?>" data-style="turquoise"></li>
                                        </ul>
                                    </div>
                                    <input name="theme_color" type="hidden" value="{{$user->theme_color}}" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-8 col-lg-offset-2 padding-top-20">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <span class="task-state pull-right padding-top-10" style="display: none"><img src="{{asset('assets/global/img/input-spinner.gif')}}" alt="Loading" /> Processing...</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END TABS -->
            <hr>

            <h3>My Orders</h3>
            <!-- TABS -->
            <div class="tab-style-1">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1b" data-toggle="tab" aria-expanded="true">Overview</a></li>
                    <li><a href="#tab-4b" data-toggle="tab">Order history</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane row fade active in" id="tab-1b">
                        <ul>
                            <li>Total order: <strong>2</strong></li>
                            <li>Latest order at: <strong>March, 23 2015</strong></li>
                        </ul>
                    </div>
                    <div class="tab-pane fade goods-data" id="tab-4b">
                        <table summary="Shopping cart" class="table-responsive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="goods-page-description">Description</th>
                                    <th class="goods-page-price" colspan="2">Total price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td class="goods-page-description padding-right-30">
                                        <h5 class="no-top-space"><a href="javascript:;">OD12387</a></h5>
                                        <p><strong>Items (2): </strong> Aggnew, ables</p>
                                    </td>
                                    <td class="goods-page-price padding-right-30">
                                        <strong><span>$</span>47.00</strong><br/>
                                        <small class="text-muted">2015/02/18 22:10</small>
                                    </td>
                                    <td class="del-goods-col">
                                        <a class="del-goods" href="javascript:;">&nbsp;</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td class="goods-page-description padding-right-30">
                                        <h5 class="no-top-space"><a href="javascript:;">OD00543</a></h5>
                                        <p><strong>Items (1): </strong> Glassive</p>
                                    </td>
                                    <td class="goods-page-price padding-right-30">
                                        <strong><span>$</span>23.00</strong>
                                    </td>
                                    <td class="del-goods-col">
                                        <a class="del-goods" href="javascript:;">&nbsp;</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END TABS -->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
@stop