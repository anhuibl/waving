@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="{{route('home')}}">Home</a></li>
    <li class="active">Restore password</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    @include('layouts.partials.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-9">
        <h1>Restore Your Password</h1>
        <div class="content-form-page">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <form action="{{action('RemindersController@postReset')}}" method="post" class="form-horizontal form-without-legend" role="form">                
                        {{Form::hidden('token', $token)}}
                        @if(!empty(Session::get('error')))
                        <h4 class="margin-bottom-25"><span class="require text-capitalize">Opps!</span> <small class="text-muted text-lowercase">{{Session::get('error')}}</small></h4>
                        @endif
                        <div class="form-group">
                            <label for="email" class="col-lg-4 control-label">Email
                                <span class="require">*</span>
                            </label>
                            <div class="col-lg-8">
                                <input name="email" value="@if(!empty(Input::old('email'))){{Input::old('email')}}@endif" type="text" class="form-control" id="email">
                            </div>
                        </div>                           
                        <div class="form-group">
                            <label for="password" class="col-lg-4 control-label">New password
                                <span class="require">*</span>
                            </label>
                            <div class="col-lg-8">
                                <input name="password" value="@if(!empty(Input::old('password'))){{Input::old('password')}}@endif" type="password" class="form-control" id="password">
                            </div>
                        </div>                   
                        <div class="form-group">
                            <label for="password_confirmation" class="col-lg-4 control-label">Confirm password
                                <span class="require">*</span>
                            </label>
                            <div class="col-lg-8">
                                <input name="password_confirmation" value="@if(!empty(Input::old('password_confirmation'))){{Input::old('password_confirmation')}}@endif" type="password" class="form-control" id="password_confirmation">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-5">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-sm-4 pull-right">
                    <div class="form-info">
                        <h2><em>Important</em> Information</h2>
                        <p>Enter the e-mail address associated with your account. Click submit to have your password e-mailed to you.</p>

                        <button type="button" class="btn btn-default">More details</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
@stop