@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="{{route('home')}}">Home</a></li>
    <li class="active">Login</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    @include('layouts.partials.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-9">
        <h1>Login</h1>
        <div class="content-form-page">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <form class="form-horizontal form-without-legend" role="form" action="{{route('login')}}" method="post">
                        {{Form::token()}}
                        @if((!empty($error) && count($error))||!empty($loginError))
                        <div class="row">
                            <div class="col-md-offset-4 col-md-8">
                                <ul class="list-items no-padding require">
                                    @if(!empty($error))
                                    @foreach($error->all() as $msg)
                                    <li>{{$msg}}</li>
                                    @endforeach
                                    @endif
                                    @if(!empty($loginError))
                                    <li>{{$loginError}}</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        @endif
                        <?php
                        $eFocus = $pFocus = "";
                        if ((!empty($error) && count($error) && $error->has('email')) || empty($error)) {
                            $eFocus = "autofocus";
                        } elseif (!empty($error) && count($error) && $error->has('password')) {
                            $pFocus = "autofocus";
                        }
                        ?>
                        <div class="form-group">
                            <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input name="email" type="text" class="form-control" id="email" {{$eFocus}} value="@if(!empty(Input::old('email'))){{Input::old('email')}}@endif">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input name="password" type="password" class="form-control" id="password" {{$pFocus}}>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-4 padding-left-0">
                                <a href="{{action('RemindersController@getRemind')}}">Forget Password?</a> or 
                                <a href="{{route('register')}}">Want a new account?</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">
                                <hr>
                                <div class="login-socio">
                                    <p class="text-muted">or login using:</p>
                                    <ul class="social-icons">
                                        <li><a href="javascript:;" data-original-title="facebook" class="facebook" title="facebook"></a></li>
                                        <li><a href="javascript:;" data-original-title="Twitter" class="twitter" title="Twitter"></a></li>
                                        <li><a href="javascript:;" data-original-title="Google Plus" class="googleplus" title="Google Plus"></a></li>
                                        <li><a href="javascript:;" data-original-title="Linkedin" class="linkedin" title="LinkedIn"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-sm-4 pull-right">
                    <div class="form-info">
                        <h2><em>Important</em> Information</h2>
                        <p>Duis autem vel eum iriure at dolor vulputate velit esse vel molestie at dolore.</p>

                        <button type="button" class="btn btn-default">More details</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
@stop