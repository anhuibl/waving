@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src='{{asset('assets/global/plugins/zoom/jquery.zoom.min.js')}}' type="text/javascript"></script><!-- product zoom -->
<script src="{{asset('assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
<script src="{{asset('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
    Layout.initImageZoom();
    Layout.initTouchspin();
    Layout.initUniform();
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="{{route('home')}}">Home</a></li>
    <li class="active">Register</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    @include('layouts.partials.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-9">
        <h1>Create an account</h1>
        <div class="content-form-page">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <form class="form-horizontal" role="form" action="{{route('register')}}" method="post">
                        @if((!empty($error) && count($error))||!empty($loginError))
                        <fieldset>
                            <legend><span class="require">Opps!</span> <small class="text-muted text-lowercase">There are some errors...</small></legend>
                            <div class="col-md-offset-4 col-md-8">
                                <ul class="list-items no-padding require">
                                    @if(!empty($error))
                                    @foreach($error->all() as $msg)
                                    <li>{{$msg}}</li>
                                    @endforeach
                                    @endif
                                    @if(!empty($loginError))
                                    <li>{{$loginError}}</li>
                                    @endif
                                </ul>
                            </div>
                        </fieldset>
                        @endif
                        <?php
                        $old = array(
                            "firstname" => "",
                            "lastname" => "",
                            "email" => "",
                        );
                        $fFocus = $lFocus = $eFocus = $pFocus = $fError = $lError = $eError = $pError = "";
                        $f = "autofocus";
                        $e = "has-error";
                        if (!empty(Input::old()) && Input::has('firstname') || !empty($error)) {
                            $old = Input::old();
                            //Check focus:
                            if ($error->has('firstname')) {
                                $fFocus = $f;
                            } elseif ($error->has('lastname')) {
                                $lFocus = $f;
                            } elseif ($error->has('email')) {
                                $eFocus = $f;
                            } elseif ($error->has('password')) {
                                $pFocus = $f;
                            }

                            //Check error class:
                            if ($error->has('firstname')) {
                                $fError = $e;
                            }
                            if ($error->has('lastname')) {
                                $lError = $e;
                            }
                            if ($error->has('email')) {
                                $eError = $e;
                            }
                            if ($error->has('password')) {
                                $pError = $e;
                            }
                        } else {
                            $fFocus = $f;
                        }
                        ?>
                        <fieldset>
                            <legend>Your personal details</legend>
                            <div class="form-group {{$fError}}">
                                <label for="firstname" class="col-lg-4 control-label">First Name <span class="require">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-icon right">
                                        @if(!empty($fError))<i class="fa fa-exclamation tooltips" data-original-title="please write a valid email" data-container="body"></i>@endif
                                        <input name="firstname" {{$fFocus}} value="{{$old['firstname']}}" type="text" class="form-control" id="firstname">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group {{$lError}}">
                                <label for="lastname" class="col-lg-4 control-label">Last Name <span class="require">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-icon right">
                                        @if(!empty($lError))<i class="fa fa-exclamation tooltips" data-original-title="please write a valid email" data-container="body"></i>@endif
                                        <input name="lastname" {{$lFocus}} value="{{$old['lastname']}}" type="text" class="form-control" id="lastname">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group {{$eError}}">
                                <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-icon right">
                                        @if(!empty($eError))<i class="fa fa-exclamation tooltips" data-original-title="please write a valid email" data-container="body"></i>@endif
                                        <input name="email" {{$eFocus}} value="{{$old['email']}}" type="text" class="form-control" id="email">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Your password</legend>
                            <div class="form-group {{$pError}}">
                                <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-icon right">
                                        @if(!empty($pError))<i class="fa fa-exclamation tooltips" data-original-title="please write a valid email" data-container="body"></i>@endif
                                        <input name="password" {{$pFocus}} type="password" class="form-control" id="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="confirm-password" class="col-lg-4 control-label">Confirm password <span class="require">*</span></label>
                                <div class="col-lg-8">
                                    <input name="password_confirmation" type="password" class="form-control" id="confirm-password">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Newsletter</legend>
                            <div class="form-group">
                                <div class="col-lg-4 control-label">
                                    <input name="newsletter" type="checkbox" id="newsletter" @if(!empty($old['newsletter'])) checked @endif>
                                </div>
                                <label for="newsletter" class="col-lg-8 margin-top-10">Singup for Newsletter </label>
                            </div>
                        </fieldset>
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                                <button type="submit" class="btn btn-primary">Create an account</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-sm-4 pull-right">
                    <div class="form-info">
                        <h2><em>Important</em> Information</h2>
                        <p>Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed sit nonumy nibh sed euismod ut laoreet dolore magna aliquarm erat sit volutpat. Nostrud exerci tation ullamcorper suscipit lobortis nisl aliquip  commodo quat.</p>

                        <p>Duis autem vel eum iriure at dolor vulputate velit esse vel molestie at dolore.</p>

                        <button type="button" class="btn btn-default">More details</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
@stop