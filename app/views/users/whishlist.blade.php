@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="{{route('home')}}">Home</a></li>
    <li class="active">My Whish List</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar col-md-3 col-sm-5">
        <ul class="list-group margin-bottom-25 sidebar-menu">
            <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Ladies</a></li>
            <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Kids</a></li>
            <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Accessories</a></li>
            <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Sports</a></li>
            <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Brands</a></li>
            <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Electronics</a></li>
            <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Home & Garden</a></li>
            <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Custom Link</a></li>
        </ul>
    </div>
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-7">
        <h1>My Wish List</h1>
        <div class="goods-page">
            <div class="goods-data clearfix">
                <div class="table-wrapper-responsive">
                    <table summary="Shopping cart">
                        <tr>
                            <th class="goods-page-image">Image</th>
                            <th class="goods-page-description">Description</th>
                            <th class="goods-page-stock">Stock</th>
                            <th class="goods-page-price" colspan="2">Unit price</th>
                        </tr>
                        <tr>
                            <td class="goods-page-image">
                                <a href="javascript:;"><img src="{{asset('assets/frontend/pages/img/products/model3.jpg')}}" alt="Berry Lace Dress"></a>
                            </td>
                            <td class="goods-page-description">
                                <h3><a href="javascript:;">Cool green dress with red bell</a></h3>
                                <p><strong>Item 1</strong> - Color: Green; Size: S</p>
                                <em>More info is here</em>
                            </td>
                            <td class="goods-page-stock">
                                In Stock
                            </td>
                            <td class="goods-page-price">
                                <strong><span>$</span>47.00</strong>
                            </td>
                            <td class="del-goods-col">
                                <a class="del-goods" href="javascript:;">&nbsp;</a>
                                <a class="add-goods" href="javascript:;">&nbsp;</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="goods-page-image">
                                <a href="javascript:;"><img src="{{asset('assets/frontend/pages/img/products/model4.jpg')}}" alt="Berry Lace Dress"></a>
                            </td>
                            <td class="goods-page-description">
                                <h3><a href="javascript:;">Cool green dress with red bell</a></h3>
                                <p><strong>Item 1</strong> - Color: Green; Size: S</p>
                                <em>More info is here</em>
                            </td>
                            <td class="goods-page-stock">
                                In Stock
                            </td>
                            <td class="goods-page-price">
                                <strong><span>$</span>47.00</strong>
                            </td>
                            <td class="del-goods-col">
                                <a class="del-goods" href="javascript:;">&nbsp;</a>
                                <a class="add-goods" href="javascript:;">&nbsp;</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
@stop