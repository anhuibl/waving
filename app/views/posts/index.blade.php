@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="{{route('home')}}">Home</a></li>
    <li class="active">News</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
        <div class="content-page">
            <div class="row">
                <!-- BEGIN LEFT SIDEBAR -->
                <div class="col-md-9 col-sm-9 blog-posts">
                    @foreach($news_categories as $cate)
                    @if(count($cate->posts))
                    <h2 class="no-top-space">{{$cate->name}} <small>@if(count($cate->posts) > 3)<i><a href="{{route('newsCategory', [$cate->show_id, Str::slug($cate->name)])}}">View all</a></i>@endif</small></h2>
                    {{View::make('posts.partials.postItem')->with(['posts' => $cate->posts->take(3)])->render()}}
                    @endif
                    @endforeach
                </div>
                <!-- END LEFT SIDEBAR -->

                <!-- BEGIN RIGHT SIDEBAR -->            
                <div class="col-md-3 col-sm-3 blog-sidebar">
                    @include('posts.partials.categoryList')

                    @include('posts.partials.recent_posts')
                    
                    <!-- BEGIN BLOG TALKS -->
                    <div class="blog-talks margin-bottom-30">
                        <h2>Popular Talks</h2>
                        <div class="tab-style-1">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1">Multipurpose</a></li>
                                <li><a data-toggle="tab" href="#tab-2">Documented</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane row-fluid fade in active">
                                    <p class="margin-bottom-10">Raw denim you probably haven't heard of them jean shorts Austin. eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p>
                                    <p><a class="more" href="javascript:;">Read more</a></p>
                                </div>
                                <div id="tab-2" class="tab-pane fade">
                                    <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. aliquip jean shorts ullamco ad vinyl aesthetic magna delectus mollit. Keytar helvetica VHS salvia..</p>
                                </div>
                            </div>
                        </div>
                    </div>                            
                    <!-- END BLOG TALKS -->

                    <!-- BEGIN BLOG PHOTOS STREAM -->
                    <div class="blog-photo-stream margin-bottom-20">
                        <h2>Photos Stream</h2>
                        <ul class="list-unstyled">
                            <li><a href="javascript:;"><img alt="" src="../../assets/frontend/pages/img/people/img5-small.jpg"></a></li>
                            <li><a href="javascript:;"><img alt="" src="../../assets/frontend/pages/img/works/img1.jpg"></a></li>
                            <li><a href="javascript:;"><img alt="" src="../../assets/frontend/pages/img/people/img4-large.jpg"></a></li>
                            <li><a href="javascript:;"><img alt="" src="../../assets/frontend/pages/img/works/img6.jpg"></a></li>
                            <li><a href="javascript:;"><img alt="" src="../../assets/frontend/pages/img/pics/img1-large.jpg"></a></li>
                            <li><a href="javascript:;"><img alt="" src="../../assets/frontend/pages/img/pics/img2-large.jpg"></a></li>
                            <li><a href="javascript:;"><img alt="" src="../../assets/frontend/pages/img/works/img3.jpg"></a></li>
                            <li><a href="javascript:;"><img alt="" src="../../assets/frontend/pages/img/people/img2-large.jpg"></a></li>
                        </ul>                    
                    </div>
                    <!-- END BLOG PHOTOS STREAM -->

                    <!-- BEGIN BLOG TAGS -->
                    <div class="blog-tags margin-bottom-20">
                        <h2>Tags</h2>
                        <ul>
                            <li><a href="javascript:;"><i class="fa fa-tags"></i>OS</a></li>
                            <li><a href="javascript:;"><i class="fa fa-tags"></i>Metronic</a></li>
                            <li><a href="javascript:;"><i class="fa fa-tags"></i>Dell</a></li>
                            <li><a href="javascript:;"><i class="fa fa-tags"></i>Conquer</a></li>
                            <li><a href="javascript:;"><i class="fa fa-tags"></i>MS</a></li>
                            <li><a href="javascript:;"><i class="fa fa-tags"></i>Google</a></li>
                            <li><a href="javascript:;"><i class="fa fa-tags"></i>Keenthemes</a></li>
                            <li><a href="javascript:;"><i class="fa fa-tags"></i>Twitter</a></li>
                        </ul>
                    </div>
                    <!-- END BLOG TAGS -->
                </div>
                <!-- END RIGHT SIDEBAR -->            
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
@stop