<!-- CATEGORIES START -->
<h2 class="no-top-space">Categories</h2>
@if(count($news_categories))
<?php if(!empty($post)) $category = $post->category; ?>
<ul class="nav sidebar-categories margin-bottom-40">
    @foreach($news_categories as $cat)
    <li @if(!empty($category) && $cat->id == $category->id)class="active"@endif><a href="@if(!empty($category) && $cat->id == $category->id)javascript:;@else{{route('newsCategory', [$cat->show_id, Str::slug($cat->name)])}}@endif">{{$cat->name}} ({{count($cat->posts)}})</a></li>
    @endforeach
</ul>
@else
<p>Not available</p>
@endif
<!-- CATEGORIES END -->