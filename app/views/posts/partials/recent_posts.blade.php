<!-- BEGIN RECENT NEWS -->
<h2>Recent News</h2>
@if(count($recent_posts))
<div class="recent-news margin-bottom-10">
    @foreach($recent_posts as $post)
    <div class="row margin-bottom-10">
        <div class="col-md-3">
            <img class="img-responsive" alt="" src="{{imageSrc($post->image_thumb, ['posts'])}}">                        
        </div>
        <div class="col-md-9 recent-news-inner">
            <h3 class="no-top-space"><a href="{{route('newsShow', [$post->show_id, Str::slug($post->title)])}}">{{$post->title}}</a></h3>
            <p>{{Str::words($post->description, 10)}}</p>
        </div>                        
    </div>
    @endforeach
</div>
@else
<p>Not available</p>
@endif
<!-- END RECENT NEWS -->