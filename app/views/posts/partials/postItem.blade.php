
@foreach($posts as $post)
<div class="row">
    <div class="col-md-4 col-sm-4">
        <?php $post->layout = "image"; ?>
        @if($post->layout == 'slider')
        <!-- BEGIN CAROUSEL -->            
        <div class="front-carousel">
            <div class="carousel slide" id="myCarousel">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="item">
                        <img alt="" src="../../assets/frontend/pages/img/works/img1.jpg">
                    </div>
                    <div class="item">
                        <img alt="" src="../../assets/frontend/pages/img/works/img2.jpg">
                    </div>
                    <div class="item active">
                        <img alt="" src="../../assets/frontend/pages/img/works/img3.jpg">
                    </div>
                </div>
                <!-- Carousel nav -->
                <a data-slide="prev" href="#myCarousel" class="carousel-control left">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a data-slide="next" href="#myCarousel" class="carousel-control right">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>                
        </div>
        <!-- END CAROUSEL -->
        @elseif($post->layout == "video")
        <!-- BEGIN VIDEO -->   
        <iframe height="205" allowfullscreen="" style="width:100%; border:0" src="http://player.vimeo.com/video/56974716?portrait=0"></iframe>
        <!-- END VIDEO -->   
        @else
        <a href="{{route('newsShow', [$post->show_id, Str::slug($post->title)])}}"><img class="img-responsive" alt="{{$post->title}}" src="{{imageSrc($post->image, ['posts'])}}"></a>
        @endif
    </div>
    <div class="col-md-8 col-sm-8">
        <h2 class="no-top-space"><a href="{{route('newsShow', [$post->show_id, Str::slug($post->title)])}}">{{$post->title}}</a></h2>
        <ul class="blog-info">
            <li><i class="fa fa-calendar"></i> {{$post->created_at->format('H:i d/m/Y')}}</li>
            <li><i class="fa fa-comments"></i> 17</li>
            <li><i class="fa fa-tags"></i> Metronic, Keenthemes, UI Design</li>
        </ul>
        <p>{{$post->description}}</p>
        <a href="{{route('newsShow', [$post->show_id, Str::slug($post->title)])}}" class="more">Read more <i class="icon-angle-right"></i></a>
    </div>
</div>
<hr class="blog-post-sep">
@endforeach
