@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src='{{asset('assets/global/plugins/zoom/jquery.zoom.min.js')}}' type="text/javascript"></script><!-- product zoom -->
<script src="{{asset('assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
<script src="{{asset('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/gmaps/gmaps.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/frontend/pages/scripts/contact-us.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
    Layout.initImageZoom();
    Layout.initTouchspin();
    Layout.initUniform();
    ContactUs.init();
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="index.html">Home</a></li>
    <li><a href="">Pages</a></li>
    <li class="active">Contact</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar col-md-3 col-sm-3">
        <ul class="list-group margin-bottom-25 sidebar-menu">
            @if(!Auth::check())
            <li class="list-group-item clearfix"><a href="{{route('login')}}"><i class="fa fa-angle-right"></i> Login</a></li>
            <li class="list-group-item clearfix"><a href="{{route('register')}}"><i class="fa fa-angle-right"></i> Register</a></li>
            <li class="list-group-item clearfix"><a href="{{action('RemindersController@getRemind')}}"><i class="fa fa-angle-right"></i> Forget Password</a></li>
            @endif
            <li class="list-group-item clearfix"><a href="{{route('account')}}"><i class="fa fa-angle-right"></i> My account</a></li>
            <li class="list-group-item clearfix"><a href="{{route('whishlist')}}"><i class="fa fa-angle-right"></i> My wish list</a></li>
            <li class="list-group-item clearfix"><a href="{{route('shoppingCart')}}"><i class="fa fa-angle-right"></i> Shopping cart</a></li>
            <li class="list-group-item clearfix"><a href="{{route('checkout')}}"><i class="fa fa-angle-right"></i> Checkout</a></li>
            @if(Auth::check())
            <li class="list-group-item clearfix"><a href="{{route('logout')}}"><i class="fa fa-angle-right"></i> Logout</a></li>
            @endif
        </ul>

        <h2>Our Contacts</h2>
        <address>
            35, Lorem Lis Street, Park Ave<br>
            California, US<br>
            <abbr title="Phone">P:</abbr> 300 323 3456<br>
        </address>
        <address>
            <strong>Email</strong><br>
            <a href="mailto:info@metronic.com">info@metronic.com</a><br>
            <a href="mailto:support@metronic.com">support@metronic.com</a>
        </address>
        <ul class="social-icons margin-bottom-10">
            <li><a href="javascript:;" data-original-title="facebook" class="facebook"></a></li>
            <li><a href="javascript:;" data-original-title="github" class="github"></a></li>
            <li><a href="javascript:;" data-original-title="Goole Plus" class="googleplus"></a></li>
            <li><a href="javascript:;" data-original-title="linkedin" class="linkedin"></a></li>
            <li><a href="javascript:;" data-original-title="rss" class="rss"></a></li>
        </ul>
    </div>
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-9">
        <h1>Contact</h1>
        <div class="content-page">
            <div id="map" class="gmaps margin-bottom-40" style="height:400px;"></div>

            <h2>Contact Form</h2>
            <p>Lorem ipsum dolor sit amet, Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat consectetuer adipiscing elit, sed diam nonummy nibh euismod tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

            <!-- BEGIN FORM-->
            <form action="#" class="default-form" role="form">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="email">Email <span class="require">*</span></label>
                    <input type="text" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" rows="8" id="message"></textarea>
                </div>
                <div class="padding-top-20">                  
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            <!-- END FORM-->          
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
@stop