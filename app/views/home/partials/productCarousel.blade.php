@foreach($items as $item)
<div>
    <div class="product-item">
        <div class="pi-img-wrapper">
            <img src="{{imageSrc($item->$image, ['products'])}}" class="img-responsive" alt="{{$item->name}}">
            <div>
                <a href="{{imageSrc($item->$image, ['products'])}}" class="btn btn-default fancybox-button">Zoom</a>
                <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
            </div>
        </div>
        <h3><a href="{{route('productsShow', [$item->show_id, Str::slug($item->name)])}}">{{$item->name}}</a></h3>
        <div class="pi-price">${{$item->promotion}}</div>
        <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
        @if(($item->promotion * 100 / $item->price) >= 50)<div class="sticker sticker-sale"></div>@endif
        @if(strtotime($item->created_at) >= (time()-3600*24))<div class="sticker sticker-new"></div>@endif
    </div>
</div>
@endforeach