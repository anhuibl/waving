@foreach($items as $item)
<div>
    <div class="product-item">
        <div class="pi-img-wrapper">
            <img src="{{imageSrc($item->$image, ['posts'])}}" class="img-responsive" alt="{{$item->title}}">
            <div>
                <a href="{{imageSrc($item->$image, ['posts'])}}" class="btn btn-default fancybox-button">Zoom</a>
            </div>
        </div>
        <h3><a href="{{route('newsShow', [$item->show_id, Str::slug($item->title)])}}">{{$item->title}}</a></h3>
        <div class="pi-price">{{$item->viewed}} <i class="fa fa-comments"></i></div>
        <a href="{{route('newsShow', [$item->show_id, Str::slug($item->title)])}}" class="btn btn-default add2cart">Read</a>
    </div>
</div>
@endforeach