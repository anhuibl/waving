@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<link href="{{asset('assets/global/plugins/rateit/src/rateit.css')}}" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src='{{asset('assets/global/plugins/zoom/jquery.zoom.min.js')}}' type="text/javascript"></script><!-- product zoom -->
<script src="{{asset('assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
<script src="{{asset('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/rateit/src/jquery.rateit.js')}}" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script><!-- for slider-range -->

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
    Layout.initImageZoom();
    Layout.initTouchspin();
    Layout.initUniform();
    Layout.initSliderRange();
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('content')
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
        <h1>Shopping cart</h1>
        <div class="goods-page">
            <div class="goods-data clearfix">
                <div class="table-wrapper-responsive">
                    <table summary="Shopping cart">
                        <tr>
                            <th class="goods-page-image">Image</th>
                            <th class="goods-page-description">Description</th>
                            <th class="goods-page-ref-no">Ref No</th>
                            <th class="goods-page-quantity">Quantity</th>
                            <th class="goods-page-price">Unit price</th>
                            <th class="goods-page-total" colspan="2">Total</th>
                        </tr>
                        <tr>
                            <td class="goods-page-image">
                                <a href="javascript:;"><img src="{{asset('assets/frontend/pages/img/products/model3.jpg')}}" alt="Berry Lace Dress"></a>
                            </td>
                            <td class="goods-page-description">
                                <h3><a href="javascript:;">Cool green dress with red bell</a></h3>
                                <p><strong>Item 1</strong> - Color: Green; Size: S</p>
                                <em>More info is here</em>
                            </td>
                            <td class="goods-page-ref-no">
                                javc2133
                            </td>
                            <td class="goods-page-quantity">
                                <div class="product-quantity">
                                    <input id="product-quantity" type="text" value="1" readonly class="form-control input-sm">
                                </div>
                            </td>
                            <td class="goods-page-price">
                                <strong><span>$</span>47.00</strong>
                            </td>
                            <td class="goods-page-total">
                                <strong><span>$</span>47.00</strong>
                            </td>
                            <td class="del-goods-col">
                                <a class="del-goods" href="javascript:;">&nbsp;</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="goods-page-image">
                                <a href="javascript:;"><img src="{{asset('assets/frontend/pages/img/products/model4.jpg')}}" alt="Berry Lace Dress"></a>
                            </td>
                            <td class="goods-page-description">
                                <h3><a href="javascript:;">Cool green dress with red bell</a></h3>
                                <p><strong>Item 1</strong> - Color: Green; Size: S</p>
                                <em>More info is here</em>
                            </td>
                            <td class="goods-page-ref-no">
                                javc2133
                            </td>
                            <td class="goods-page-quantity">
                                <div class="product-quantity">
                                    <input id="product-quantity2" type="text" value="1" readonly class="form-control input-sm">
                                </div>
                            </td>
                            <td class="goods-page-price">
                                <strong><span>$</span>47.00</strong>
                            </td>
                            <td class="goods-page-total">
                                <strong><span>$</span>47.00</strong>
                            </td>
                            <td class="del-goods-col">
                                <a class="del-goods" href="javascript:;">&nbsp;</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="shopping-total">
                    <ul>
                        <li>
                            <em>Sub total</em>
                            <strong class="price"><span>$</span>47.00</strong>
                        </li>
                        <li>
                            <em>Shipping cost</em>
                            <strong class="price"><span>$</span>3.00</strong>
                        </li>
                        <li class="shopping-total-price">
                            <em>Total</em>
                            <strong class="price"><span>$</span>50.00</strong>
                        </li>
                    </ul>
                </div>
            </div>
            <button class="btn btn-default" type="submit">Continue shopping <i class="fa fa-shopping-cart"></i></button>
            <button class="btn btn-primary" type="submit">Checkout <i class="fa fa-check"></i></button>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->

<!-- BEGIN SIMILAR PRODUCTS -->
<div class="row margin-bottom-40">
    <div class="col-md-12 col-sm-12">
        <h2>Most popular products</h2>
        <div class="owl-carousel owl-carousel4">
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k1.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k1.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                    <div class="sticker sticker-new"></div>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k2.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k2.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress2</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k3.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k3.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress3</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k4.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k4.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress4</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                    <div class="sticker sticker-sale"></div>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k1.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k1.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress5</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k2.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k2.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress6</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SIMILAR PRODUCTS -->
@stop