<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->

    <!-- Head BEGIN -->
    <head>
        <meta charset="utf-8">
        <title>CraYi - You and I, together be crazy</title>

        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta content="It is our home, where we are belong to. A world that we can respect, love and develop ourself, others and life." name="description">
        <meta content="personal development, belong to, our home, love, respect, love life, live, planning, to do list" name="keywords">
        <meta content="boloong anhui" name="author">

        <meta property="og:site_name" content="CraYi">
        <meta property="og:title" content="Pesonal development world">
        <meta property="og:description" content="It is our home, where we are belong to. A world that we can respect, love and develop ourself, others and life.">
        <meta property="og:type" content="website">
        <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
        <meta property="og:url" content="{{Request::url()}}">

        <link rel="shortcut icon" href="{{url("favicon.ico")}}">
        <link rel="alternate" hreflang="en" href="{{Request::url()}}" />

        <!-- Fonts START -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css"><!--- fonts for slider on the index page -->  
        <!-- Fonts END -->

        <!-- Global styles START -->          
        <link href="{{asset('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Global styles END -->

        @yield('top_assets') 

        <!-- Theme styles START -->
        <link href="{{asset('assets/global/css/components.css')}}" rel="stylesheet">
        <link href="{{asset('assets/frontend/layout/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('assets/frontend/pages/css/style-shop.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/frontend/pages/css/style-layer-slider.css')}}" rel="stylesheet">
        <link href="{{asset('assets/frontend/layout/css/style-responsive.css')}}" rel="stylesheet">
        <?php
        $theme = "red";
        if (Auth::check() && Auth::user()->theme_color != "") {
            $theme = Auth::user()->theme_color;
        }
        ?>
        <link href="{{asset('assets/frontend/layout/css/themes/'.$theme.'.css')}}" rel="stylesheet" id="style-color">
        <link href="{{asset('assets/frontend/layout/css/custom.css')}}" rel="stylesheet">
        <link href="{{asset('assets/main.css')}}" rel="stylesheet">
        <!-- Theme styles END -->
    </head>
    <!-- Head END -->

    <!-- Body BEGIN -->
    <body class="ecommerce">
        @include('layouts.partials.ggAnalytics')
        @include('layouts.partials.style_customizer')
        @include('layouts.partials.top_bar')
        @include('layouts.partials.header')
        @yield('top_main')

        <div class="main">
            <div class="container">
                @yield('content')
            </div>
        </div>
        
        @yield('bottom_main')
        @include('layouts.partials.pre_footer')
        @include('layouts.partials.footer')

        <!-- BEGIN fast view of a product -->
        <div id="product-pop-up" style="display: none; width: 700px;">
            <div class="product-page product-pop-up">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-3">
                        <div class="product-main-image">
                            <img src="{{asset('assets/frontend/pages/img/products/model7.jpg')}}" alt="Cool green dress with red bell" class="img-responsive">
                        </div>
                        <div class="product-other-images">
                            <a href="javascript:;" class="active"><img alt="Berry Lace Dress" src="{{asset('assets/frontend/pages/img/products/model3.jpg')}}"></a>
                            <a href="javascript:;"><img alt="Berry Lace Dress" src="{{asset('assets/frontend/pages/img/products/model4.jpg')}}"></a>
                            <a href="javascript:;"><img alt="Berry Lace Dress" src="{{asset('assets/frontend/pages/img/products/model5.jpg')}}"></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-9">
                        <h2>Cool green dress with red bell</h2>
                        <div class="price-availability-block clearfix">
                            <div class="price">
                                <strong><span>$</span>47.00</strong>
                                <em>$<span>62.00</span></em>
                            </div>
                            <div class="availability">
                                Availability: <strong>In Stock</strong>
                            </div>
                        </div>
                        <div class="description">
                            <p>Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed nonumy nibh sed euismod laoreet dolore magna aliquarm erat volutpat 
                                Nostrud duis molestie at dolore.</p>
                        </div>
                        <div class="product-page-options">
                            <div class="pull-left">
                                <label class="control-label">Size:</label>
                                <select class="form-control input-sm">
                                    <option>L</option>
                                    <option>M</option>
                                    <option>XL</option>
                                </select>
                            </div>
                            <div class="pull-left">
                                <label class="control-label">Color:</label>
                                <select class="form-control input-sm">
                                    <option>Red</option>
                                    <option>Blue</option>
                                    <option>Black</option>
                                </select>
                            </div>
                        </div>
                        <div class="product-page-cart">
                            <div class="product-quantity">
                                <input id="product-quantity" type="text" value="1" readonly name="product-quantity" class="form-control input-sm">
                            </div>
                            <button class="btn btn-primary" type="submit">Add to cart</button>
                            <a href="shop-item.html" class="btn btn-default">More details</a>
                        </div>
                    </div>

                    <div class="sticker sticker-sale"></div>
                </div>
            </div>
        </div>
        <!-- END fast view of a product -->

        <!-- Load javascripts at bottom, this will reduce page load time -->
        <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
        <!--[if lt IE 9]>
        <script src="{{asset('assets/global/plugins/respond.min.js')}}"></script>  
        <![endif]-->
        <script src="{{asset('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>      
        <script src="{{asset('assets/frontend/layout/scripts/back-to-top.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/custom.js')}}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

        <script type="text/javascript">
$(document).ready(function () {
<?php if (!(Auth::check() && !Auth::user()->fixed_header)): ?>
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
<?php endif; ?>
});
        </script>
        @yield('bottom_assets')
    </body>
    <!-- END BODY -->
</html>