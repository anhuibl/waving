
<!-- BEGIN HEADER -->
<div class="header">
    <div class="container">
        <h1 class="no-space">
            <a class="site-logo" href="{{route('home')}}">
                WAVING<small class="text-muted text-lowercase">shop</small><i class="fa fa-pied-piper-alt"></i>
            </a>
        </h1>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN CART -->
        <div class="top-cart-block">
            <div class="top-cart-info">
                <a href="javascript:void(0);" class="top-cart-info-count">3 items</a>
                <a href="javascript:void(0);" class="top-cart-info-value">$1260</a>
            </div>
            <i class="fa fa-shopping-cart"></i>

            <div class="top-cart-content-wrapper">
                <div class="top-cart-content">
                    <ul class="scroller" style="height: 250px;">
                        <li>
                            <a href="{{route('productsShow', array('category', 'item'))}}"><img src="{{asset('assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                            <span class="cart-content-count">x 1</span>
                            <strong><a href="{{route('productsShow', array('category', 'item'))}}">Rolex Classic Watch</a></strong>
                            <em>$1230</em>
                            <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
                        </li>
                        <li>
                            <a href="{{route('productsShow', array('category', 'item'))}}"><img src="{{asset('assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                            <span class="cart-content-count">x 1</span>
                            <strong><a href="{{route('productsShow', array('category', 'item'))}}">Rolex Classic Watch</a></strong>
                            <em>$1230</em>
                            <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
                        </li>
                        <li>
                            <a href="{{route('productsShow', array('category', 'item'))}}"><img src="{{asset('assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                            <span class="cart-content-count">x 1</span>
                            <strong><a href="{{route('productsShow', array('category', 'item'))}}">Rolex Classic Watch</a></strong>
                            <em>$1230</em>
                            <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
                        </li>
                        <li>
                            <a href="{{route('productsShow', array('category', 'item'))}}"><img src="{{asset('assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                            <span class="cart-content-count">x 1</span>
                            <strong><a href="{{route('productsShow', array('category', 'item'))}}">Rolex Classic Watch</a></strong>
                            <em>$1230</em>
                            <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
                        </li>
                        <li>
                            <a href="{{route('productsShow', array('category', 'item'))}}"><img src="{{asset('assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                            <span class="cart-content-count">x 1</span>
                            <strong><a href="{{route('productsShow', array('category', 'item'))}}">Rolex Classic Watch</a></strong>
                            <em>$1230</em>
                            <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
                        </li>
                        <li>
                            <a href="{{route('productsShow', array('category', 'item'))}}"><img src="{{asset('assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                            <span class="cart-content-count">x 1</span>
                            <strong><a href="{{route('productsShow', array('category', 'item'))}}">Rolex Classic Watch</a></strong>
                            <em>$1230</em>
                            <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
                        </li>
                        <li>
                            <a href="{{route('productsShow', array('category', 'item'))}}"><img src="{{asset('assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                            <span class="cart-content-count">x 1</span>
                            <strong><a href="{{route('productsShow', array('category', 'item'))}}">Rolex Classic Watch</a></strong>
                            <em>$1230</em>
                            <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
                        </li>
                        <li>
                            <a href="{{route('productsShow', array('category', 'item'))}}"><img src="{{asset('assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                            <span class="cart-content-count">x 1</span>
                            <strong><a href="{{route('productsShow', array('category', 'item'))}}">Rolex Classic Watch</a></strong>
                            <em>$1230</em>
                            <a href="javascript:void(0);" class="del-goods">&nbsp;</a>
                        </li>
                    </ul>
                    <div class="text-right">
                        <a href="{{route('shoppingCart')}}" class="btn btn-default">View Cart</a>
                        <a href="{{route('checkout')}}" class="btn btn-primary">Checkout</a>
                    </div>
                </div>
            </div>            
        </div>
        <!--END CART -->

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation">
            <ul>
                <li {{checkAttr(Route::currentRouteName(),['home'])}}><a href="{{route('home')}}">Home</a></li>
                <li class="dropdown dropdown-megamenu {{checkAttr(Route::currentRouteName(),['products', 'productsCategory', 'productsShow'], true)}}">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
                        Products

                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="header-navigation-content">
                                <div class="row">
                                    <div class="col-md-4 header-navigation-col">
                                        <h4>Footwear</h4>
                                        <ul>
                                            <li><a href="{{route('productsCategory', 'category')}}">Astro Trainers</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Basketball Shoes</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Boots</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Canvas Shoes</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Football Boots</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Golf Shoes</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Hi Tops</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Indoor and Court Trainers</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 header-navigation-col">
                                        <h4>Clothing</h4>
                                        <ul>
                                            <li><a href="{{route('productsCategory', 'category')}}">Base Layer</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Character</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Chinos</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Combats</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Cricket Clothing</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Fleeces</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Gilets</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Golf Tops</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 header-navigation-col">
                                        <h4>Accessories</h4>
                                        <ul>
                                            <li><a href="{{route('productsCategory', 'category')}}">Belts</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Caps</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Gloves, Hats and Scarves</a></li>
                                        </ul>

                                        <h4>Clearance</h4>
                                        <ul>
                                            <li><a href="{{route('productsCategory', 'category')}}">Jackets</a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}">Bottoms</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 nav-brands">
                                        <ul>
                                            <li><a href="{{route('productsCategory', 'category')}}"><img title="esprit" alt="esprit" src="{{asset('assets/frontend/pages/img/brands/esprit.jpg')}}"></a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}"><img title="gap" alt="gap" src="{{asset('assets/frontend/pages/img/brands/gap.jpg')}}"></a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}"><img title="next" alt="next" src="{{asset('assets/frontend/pages/img/brands/next.jpg')}}"></a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}"><img title="puma" alt="puma" src="{{asset('assets/frontend/pages/img/brands/puma.jpg')}}"></a></li>
                                            <li><a href="{{route('productsCategory', 'category')}}"><img title="zara" alt="zara" src="{{asset('assets/frontend/pages/img/brands/zara.jpg')}}"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                @if(count($sales))
                <li class="dropdown dropdown100 nav-catalogue {{checkAttr(Route::currentRouteName(),['sales'], true)}}">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
                        Sales

                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="header-navigation-content">
                                <div class="row">
                                    @foreach($sales as $sale)
                                    <div class="col-md-3 col-sm-4 col-xs-6">
                                        <div class="product-item">
                                            <div class="pi-img-wrapper">
                                                <a href="{{route('productsShow', [$sale->show_id, Str::slug($sale->name)])}}"><img src="{{imageSrc($sale->image, ['products'])}}" class="img-responsive" alt="{{$sale->name}}"></a>
                                            </div>
                                            <h3><a href="{{route('productsShow', [$sale->show_id, Str::slug($sale->name)])}}">{{$sale->name}} <span class="require">{{number_format($sale->saling, 1)}} %</span></a></h3>
                                            <div class="pi-price">${{$sale->promotion?$sale->promotion:$sale->price}}</div>
                                            <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                @endif
                <li class="dropdown {{checkAttr(Route::currentRouteName(),['news', 'newsCategory', 'newsShow'], true)}}">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
                        News 

                    </a>

                    <ul class="dropdown-menu">
                        @foreach($news_categories as $news_category)
                        <li><a href="{{route('newsCategory', [$news_category->show_id, Str::slug($news_category->name)])}}">{{$news_category->name}}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li {{checkAttr(Route::currentRouteName(),['aboutUs'])}}><a href="{{route('aboutUs')}}">About</a></li>
                <li {{checkAttr(Route::currentRouteName(),['contactUs'])}}><a href="{{route('contactUs')}}">Contact us</a></li>

                <!-- BEGIN TOP SEARCH -->
                <li class="menu-search">
                    <span class="sep"></span>
                    <i class="fa fa-search search-btn"></i>
                    <div class="search-box">
                        <form action="{{route('search')}}" method="post">
                            <div class="input-group">
                                <input type="text" placeholder="Search" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                </span>
                            </div>
                        </form>
                    </div> 
                </li>
                <!-- END TOP SEARCH -->
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>
<!-- Header END -->