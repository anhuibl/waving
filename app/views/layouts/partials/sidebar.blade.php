<!-- BEGIN SIDEBAR -->
<div class="sidebar col-md-3 col-sm-3">
    <ul class="list-group margin-bottom-25 sidebar-menu">
        @if(!Auth::check())
        <li class="list-group-item clearfix"><a href="{{route('login')}}"><i class="fa fa-angle-right"></i> Login</a></li>
        <li class="list-group-item clearfix"><a href="{{route('register')}}"><i class="fa fa-angle-right"></i> Register</a></li>
        <li class="list-group-item clearfix"><a href="{{action('RemindersController@getRemind')}}"><i class="fa fa-angle-right"></i> Forget Password</a></li>
        @endif
        <li class="list-group-item clearfix"><a href="{{route('account')}}"><i class="fa fa-angle-right"></i> My account</a></li>
        <li class="list-group-item clearfix"><a href="{{route('whishlist')}}"><i class="fa fa-angle-right"></i> My wish list</a></li>
        <li class="list-group-item clearfix"><a href="{{route('shoppingCart')}}"><i class="fa fa-angle-right"></i> Shopping cart</a></li>
        <li class="list-group-item clearfix"><a href="{{route('checkout')}}"><i class="fa fa-angle-right"></i> Checkout</a></li>
        @if(Auth::check())
        <li class="list-group-item clearfix"><a href="{{route('logout')}}"><i class="fa fa-angle-right"></i> Logout</a></li>
        @endif
    </ul>
</div>
<!-- END SIDEBAR -->