@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css')}}" rel="stylesheet" type="text/css"><!-- for slider-range -->
<link href="{{asset('assets/global/plugins/rateit/src/rateit.css')}}" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src='{{asset('assets/global/plugins/zoom/jquery.zoom.min.js')}}' type="text/javascript"></script><!-- product zoom -->
<script src="{{asset('assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
<script src="{{asset('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/rateit/src/jquery.rateit.js')}}" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script><!-- for slider-range -->

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    Layout.initTwitter();
    Layout.initImageZoom();
    Layout.initTouchspin();
    Layout.initUniform();
    Layout.initSliderRange();
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
@stop

@section('top_main')
<div class="title-wrapper">
    <div class="container"><div class="container-inner">
            <h1>{{$category->name}}</h1>
            <em>Over {{$category->products->count()}} Items are available here</em>
        </div></div>
</div>
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="{{route('home')}}">Home</a></li>
    <li><a href="{{route('products')}}">Products</a></li>
    <li class="active">{{$category->name}}</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar col-md-3 col-sm-5">

        <ul class="list-group margin-bottom-25 sidebar-menu">
            @foreach ($categories as $cat)
            @if ($cat->id == $category->id) <?php $cat->current = $cat->id; ?> @else <?php $cat->current = '0'; ?> @endif
            @endforeach
            @each('products.partials.categoryList', $categories, 'categories', 'products.partials.categoryNone')
        </ul>

        <div class="sidebar-filter margin-bottom-25">
            <h2>Filter</h2>
            <h3>Availability</h3>
            <div class="checkbox-list">
                <label><input type="checkbox"> Not Available (3)</label>
                <label><input type="checkbox"> In Stock (26)</label>
            </div>

            <h3>Price</h3>
            <p>
                <label for="amount">Range:</label>
                <input type="text" id="amount" style="border:0; color:#f6931f; font-weight:bold;">
            </p>
            <div id="slider-range"></div>
        </div>

        <div class="sidebar-products clearfix">
            <h2>Bestsellers</h2>
            @each('products.partials.bestSellers', $bestSellers = [20.00, 45.12, 33.76], 'bestSellers')
        </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-7">
        <div class="row list-view-sorting clearfix">
            <div class="col-md-2 col-sm-2 list-view">
                <a href="javascript:;"><i class="fa fa-th-large"></i></a>
                <a href="javascript:;"><i class="fa fa-th-list"></i></a>
            </div>
            <div class="col-md-10 col-sm-10">
                <div class="pull-right">
                    <label class="control-label">Show:</label>
                    <select class="form-control input-sm">
                        <option value="#?limit=24" selected="selected">24</option>
                        <option value="#?limit=25">25</option>
                        <option value="#?limit=50">50</option>
                        <option value="#?limit=75">75</option>
                        <option value="#?limit=100">100</option>
                    </select>
                </div>
                <div class="pull-right">
                    <label class="control-label">Sort&nbsp;By:</label>
                    <select class="form-control input-sm">
                        <option value="#?sort=p.sort_order&amp;order=ASC" selected="selected">Default</option>
                        <option value="#?sort=pd.name&amp;order=ASC">Name (A - Z)</option>
                        <option value="#?sort=pd.name&amp;order=DESC">Name (Z - A)</option>
                        <option value="#?sort=p.price&amp;order=ASC">Price (Low &gt; High)</option>
                        <option value="#?sort=p.price&amp;order=DESC">Price (High &gt; Low)</option>
                        <option value="#?sort=rating&amp;order=DESC">Rating (Highest)</option>
                        <option value="#?sort=rating&amp;order=ASC">Rating (Lowest)</option>
                        <option value="#?sort=p.model&amp;order=ASC">Model (A - Z)</option>
                        <option value="#?sort=p.model&amp;order=DESC">Model (Z - A)</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- BEGIN PRODUCT LIST -->
        {{View::make('products.partials.productItem', ['items' => $category->products, 'image' => 'image'])->render()}}
        <!-- END PRODUCT LIST -->
        <!-- BEGIN PAGINATOR -->
        <div class="row">
            <div class="col-md-4 col-sm-4 items-info">Items 1 to 9 of 10 total</div>
            <div class="col-md-8 col-sm-8">
                <ul class="pagination pull-right">
                    <li><a href="javascript:;">&laquo;</a></li>
                    <li><a href="javascript:;">1</a></li>
                    <li><span>2</span></li>
                    <li><a href="javascript:;">3</a></li>
                    <li><a href="javascript:;">4</a></li>
                    <li><a href="javascript:;">5</a></li>
                    <li><a href="javascript:;">&raquo;</a></li>
                </ul>
            </div>
        </div>
        <!-- END PAGINATOR -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
@stop