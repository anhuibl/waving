@if (count($categories->childrens))
<li class="list-group-item clearfix dropdown {{checkAttr($categories->id, [$categories->current], true)}}">
    <a href="javascript:void(0);" class="collapsed"><i class="fa fa-angle-right"></i> {{ $categories->name }}</a>
    <ul class="dropdown-menu" style="display:block;">
        @each('products.partials.categoryList', $categories->childrens, 'categories', 'products.partials.categoryNone')
    </ul>
</li>
@else
<li class="list-group-item clearfix {{checkAttr($categories->id, [$categories->current], true)}}">
    <a href="{{route("productsCategory", [$categories->show_id, Str::slug($categories->name)])}}"><i class="fa fa-angle-right"></i> {{ $categories->name }}</a>
</li>
@endif