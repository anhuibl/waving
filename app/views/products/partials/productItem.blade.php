<?php $count = 0; ?>
@foreach($items as $item)
@if($count % 3 == 0 && $count != 0)</div>@endif
@if($count % 3 == 0)<div class="row product-list">@endif
<!-- PRODUCT ITEM START -->
<div class="col-md-4 col-sm-12 col-xs-12">
    <div class="product-item">
        <div class="pi-img-wrapper">
            <img src="{{imageSrc($item->$image, ['products'])}}" class="img-responsive" alt="{{$item->name}}">
            <div>
                <a href="{{imageSrc($item->$image, ['products'])}}" class="btn btn-default fancybox-button">Zoom</a>
                <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
            </div>
        </div>
        <h3><a href="{{route('productsShow', [$item->show_id, Str::slug($item->name)])}}">{{$item->name}}</a></h3>
        <div class="pi-price">${{$item->promotion?$item->promotion:$item->price}}</div>
        <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
        @if($item->promotion != 0)<div class="sticker sticker-sale"></div>@endif
        @if(strtotime($item->created_at) >= (time()-3600*24))<div class="sticker sticker-new"></div>@endif
    </div>
</div>              
<!-- PRODUCT ITEM END -->
<?php $count++; ?>
@if($count == count($items))</div>@endif
@endforeach