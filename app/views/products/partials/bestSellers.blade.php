<div class="item">
    <a href="{{Route('productsShow', [$bestSellers, 'name'])}}"><img src="{{asset('assets/frontend/pages/img/products/k1.jpg')}}" alt="Some Shoes in Animal with Cut Out"></a>
    <h3><a href="{{Route('productsShow', [$bestSellers, 'name'])}}">Some Shoes in Animal with Cut Out</a></h3>
    <div class="price">${{$bestSellers}}</div>
</div>