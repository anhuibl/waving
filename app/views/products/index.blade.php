@extends('layouts.main')

@section('top_assets')
<!-- Page level plugin styles START -->
<link href="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
<!-- Page level plugin styles END -->
@stop

@section('bottom_assets')
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src='{{asset('assets/global/plugins/zoom/jquery.zoom.min.js')}}' type="text/javascript"></script><!-- product zoom -->
<script src="{{asset('assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->

<!-- BEGIN LayerSlider -->
<script src="{{asset('assets/global/plugins/slider-layer-slider/js/greensock.js')}}" type="text/javascript"></script><!-- External libraries: GreenSock -->
<script src="{{asset('assets/global/plugins/slider-layer-slider/js/layerslider.transitions.js')}}" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="{{asset('assets/global/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js')}}" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="{{asset('assets/frontend/pages/scripts/layerslider-init.js')}}" type="text/javascript"></script>
<!-- END LayerSlider -->

<script src="{{asset('assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    Layout.init();
    Layout.initOWL();
    LayersliderInit.initLayerSlider();
    Layout.initImageZoom();
    Layout.initTouchspin();
    Layout.initTwitter();
});
</script>
@stop

@section('content')
<ul class="breadcrumb">
    <li><a href="{{route('home')}}">Home</a></li>
    <li class="active">Products</li>
</ul>
<!-- BEGIN TWO PRODUCTS & PROMO -->
<div class="row margin-bottom-35 ">
    <!-- BEGIN TWO PRODUCTS -->
    <div class="col-md-6 two-items-bottom-items">
        <h2>Best sellers</h2>
        <div class="owl-carousel owl-carousel2">
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k4.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k4.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k2.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k2.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k3.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k3.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k1.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k1.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k4.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k4.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
            <div>
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('assets/frontend/pages/img/products/k3.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                        <div>
                            <a href="{{asset('assets/frontend/pages/img/products/k3.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">Berry Lace Dress</a></h3>
                    <div class="pi-price">$29.00</div>
                    <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
            </div>
        </div>
    </div>
    <!-- END TWO PRODUCTS -->
    <!-- BEGIN PROMO -->
    <div class="col-md-6 shop-index-carousel">
        <div class="content-slider">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{{asset('assets/frontend/pages/img/index-sliders/slide1.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/frontend/pages/img/index-sliders/slide2.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/frontend/pages/img/index-sliders/slide3.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROMO -->
</div>        
<!-- END TWO PRODUCTS & PROMO -->

@foreach($categories as $category)
@if(count($category->products))
<!-- BEGIN SALE PRODUCT & NEW ARRIVALS -->
<div class="row margin-bottom-40">
    <!-- BEGIN SALE PRODUCT -->
    <div class="col-md-12 sale-product">
        <h2>{{$category->name}} <small><i><a href="{{route('productsCategory', [$category->show_id, Str::slug($category->name)])}}">View all</a></i></small></h2>
        <div class="owl-carousel owl-carousel5">
            {{View::make("home.partials.productCarousel")->with(["items" => $category->products, 'image' => 'image'])->render()}}
        </div>
    </div>
    <!-- END SALE PRODUCT -->
</div>
<!-- END SALE PRODUCT & NEW ARRIVALS -->
@endif
@endforeach
@stop