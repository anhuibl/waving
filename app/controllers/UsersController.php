<?php

class UsersController extends BaseController {

    protected $user;

    public function __construct(User $user) {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * Index page - Show user details
     * @return view
     */
    public function index() {
        $user = Auth::user();
        return View::make('users.account')->with('user', $user);
    }

    /**
     * Login page
     * @return view
     */
    public function login() {
        if (Request::isMethod('post')) {
            $input = eInputs(Input::only('email', 'password'));
            $v = $this->user->validator($input, "login");
            if ($v->passes()) {
                if (Auth::attempt(['email' => $input['email'], 'password' => $input['password'], 'status' => 1])) {
                    return Redirect::route('account');
                } else {
                    Input::flash();
                    return View::make('users.login')->with('loginError', 'Email or password is incorrect!');
                }
            } else {
                Input::flash();
                return View::make('users.login')->with('error', $v->messages());
            }
        } else {
            return View::make('users.login');
        }
    }

    /**
     * Logout page
     * @return view
     */
    public function logout() {
        if (Auth::check()) {
            Auth::logout();
            return Redirect::route('home');
        }
        return Redirect::route('login');
    }

    /**
     * Register page - Regis new guest account
     * @return view
     */
    public function register() {
        if (Request::isMethod('post')) {
            $input = eInputs(Input::all());
            $v = $this->user->validator($input);
            if ($v->passes()) {
                if (!empty($input['newsletter'])) {
                    //Process newsletter
                    unset($input['newsletter']);
                }
                $input['password'] = Hash::make($input['password']);
                unset($input['password_confirmation']);
                $user = new User;
                foreach ($input as $key => $value) {
                    $user->$key = $value;
                }
                if ($user->save()) {
                    Auth::login($user);
                    return Redirect::route('account');
                } else {
                    Input::flash();
                    return View::make('users.register')->with('loginError', 'Errors, please try again!');
                }
            } else {
                Input::flash();
                return View::make('users.register')->with('error', $v->messages());
            }
        } else {
            return View::make('users.register');
        }
    }

    /**
     * Update login user data: profile/password/setting
     * @param type $type
     * @return json - data.success, data.msg or data.errors
     */
    public function update($type = "profile") {
        $user = Auth::user();
        parse_str(Input::get('formData'), $input);
        $input = eInputs($input);
        $validate = $this->user->validator($input, $type);
        if ($type == "profile") {
            if ($validate->passes()) {                  //Proccess change profile
                foreach ($input as $key => $value) {
                    $user->$key = $value;
                }
                return $this->jsonProcessor($user->save(), "Profile changed.", "Error, please try later.");
            } else {
                return $this->jsonFailErrors($validate->messages());
            }
        } elseif ($type == "password") {                //Proccess change password
            $passMatch = Hash::check($input['current_password'], $user->password);
            $user->password = Hash::make($input['password']);
            if ($validate->passes()) {
                return $this->jsonProcessor($passMatch && $user->save(), "Password changed.", "The current password is incorrect.");
            } else {
                return $this->jsonFailErrors($validate->messages());
            }
        } elseif ($type == "setting") {                 //Proccess change settings
            if (!empty($input['fixed_header'])) {
                $input['fixed_header'] = 1;
            } else {
                $input['fixed_header'] = 0;
            }
            $user->theme_color = $input['theme_color'];
            $user->fixed_header = $input['fixed_header'];
            return $this->jsonProcessor($user->save(), "Setting changed.", "Error, please try later.");
        } elseif ($type == "avatar") {
            if (Input::hasFile("avatar")) {
                $image = Input::file("avatar");
                $fileName = md5("avatar" . $user->id) . "." . $image->getClientOriginalExtension();
                $to = "uploads/users";
                $image->move($to, $fileName);
                $avatar = Image::make($to . "/" . $fileName);
                $avatar->crop(768, 680);
                $avatar->save();
                $user->avatar = $fileName;
                $user->save();
                return Redirect::back()->with("status", "Avatar changed!");
            } else {
                return Redirect::back()->with("error", "Avatar must be an image.");
            }
            //return $this->jsonProcessor($user->save(), "Setting changed.", "Error, please try later.");
        } else {                                        //Response error
            return $this->jsonFailMsg("Invalid type.");
        }
    }

    /**
     * Whishlist page - Reset password page
     * @return view
     */
    public function whishlist() {
        return View::make('users.whishlist');
    }

}
