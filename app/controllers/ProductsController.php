<?php

class ProductsController extends BaseController {
    
    function __construct() {
        parent::__construct();
    }

    /**
     * Index page - List category with it's featured products
     * @return view
     */
    public function index() {
        $categories = Category::with(['products' => function($q) {
                $q->where(['status' => 1]);
            },'childrens' => function($q){
                $q->where(['status' => 1])->select(['parent_id', 'id']);
            },'childrens.products' => function($q) {
                $q->where(['status' => 1]);
            }])->where(['status' => 1, 'parent_id' => 0])->get();
        foreach ($categories as $category) {
            foreach ($category->childrens as $childrens) {
                if(count($childrens->products)){
                    $category->products = $category->products->merge($childrens->products);
                }
            }
        }
        return View::make('products.index')->with(['categories' => $categories]);
    }

    /**
     * Category page - List product by category
     * @return view
     */
    public function category($id) {
        $category = getIdData($id, 'Category', ['products' => function($q) {
                $q->where(['status' => 1]);
            },'childrens' => function($q){
                $q->where(['status' => 1])->select(['parent_id', 'id']);
            },'childrens.products' => function($q) {
                $q->where(['status' => 1]);
            }], ['status' => 1]);
        foreach ($category->childrens as $childrens) {
            if(count($childrens->products)){
                $category->products = $category->products->merge($childrens->products);
            }
        }  
        $categories = Category::with("childrens")->where(array("status" => 1, "parent_id" => 0))->get();
        return View::make('products.category')->with(array("categories" => $categories, 'category' => $category));
    }

            /**
             * Show page - Show product details by alias
             * @return view
             */
    public function show($id) {
        $product = getIdData($id, "Product", ['category'], ['status' => 1]);
        $product->viewed = $product->viewed + 1;
        $product->save();
        $categories = Category::with("childrens")->where(array("status" => 1, "parent_id" => 0))->get();
        return View::make('products.show')->with(['product' => $product, 'categories' => $categories]);
    }

}
        