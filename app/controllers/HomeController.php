<?php

class HomeController extends BaseController {
    
    function __construct() {
        parent::__construct();
    }

    /**
     * Home page
     * @return view
     */
    public function index() {
        $newProducts = Product::where("status", 1)->orderBy("created_at", "desc")->limit(10)->get();
        $hotNews = Post::where("hot", 1)->orderBy("viewed", "desc")->limit(10)->get();
        $featuredProducts = Product::where(["status" => 1, 'hot' => 1])->orderBy("viewed", "desc")->limit(10)->get();
        $categories = Category::with("childrens")->where(array("status" => 1, "parent_id" => 0))->get();
        return View::make('home.index')->with(["newProducts" => $newProducts, 'categories' => $categories, 'featuredProducts' => $featuredProducts, 'hotNews' => $hotNews]);
    }

    /**
     * Home page
     * @return view
     */
    public function search() {
        return View::make('home.search');
    }

    /**
     * About page
     * @return view
     */
    public function aboutUs() {
        return View::make('home.aboutUs');
    }

    /**
     * Contact page
     * @return view
     */
    public function contactUs() {
        return View::make('home.contactUs');
    }

    /**
     * Privacy policy page
     * @return view
     */
    public function privacyPolicy() {
        return View::make('home.privacyPolicy');
    }

    /**
     * Terms and conditions page
     * @return view
     */
    public function termsConditions() {
        return View::make('home.termsConditions');
    }

    /**
     * Faq page
     * @return view
     */
    public function faq() {
        return View::make('home.faq');
    }
    
    public function notFound() {
	return View::make("layouts.partials.404");
    }
    
    public function test() {
        return View::make('home.test');
    }

}
