<?php

class PostsController extends BaseController {
    
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Index page - List category with it's featured posts
     * @return view
     */
    public function index() {
        return View::make('posts.index');
    }

    /**
     * Category page - List post by category
     * @return view
     */
    public function category($id) {
        $items_per_page = 5;
        if(Request::isMethod('get')){
            $category = getIdData($id, 'PostCategory', ['posts' => function($q) use ($items_per_page) {
                $q->where('status', 1)->orderBy('viewed', 'desc')->select(['id', 'created_at', 'show_id', 'title', 'description', 'post_category_id', 'image'])->take($items_per_page);
            }], ['status' => 1]);
            $total_item = Post::where('post_category_id', $category->id)->count();
            $total_page = ceil($total_item/$items_per_page);
        } else {
            $data = Input::all();
            $posts = Post::where(['post_category_id' => $id])->skip($items_per_page*$data['current'])->take($items_per_page)->get();
            if (count($posts)) {
                return Response::json(['success' => true, 'html' => View::make('posts.partials.postItem')->with(['posts' => $posts])->render()]);
            } else {
                return Response::json(['success' => false, 'error' => 'No item left.']);
            }
        }
        return View::make('posts.category')->with(['category' => $category, 'total_page' => $total_page]);
    }

    /**
     * Show page - Show post details by alias
     * @return view
     */
    public function show($id) {
        $post = getIdData($id, 'Post', ['category' => function($q){
            $q->select(['id', 'show_id', 'name']);
        }], ['status' => 1]);
        return View::make('posts.show')->with(['post' => $post]);
    }

}
