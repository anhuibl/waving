<?php

class RemindersController extends BaseController {
    
    function __construct() {
        parent::__construct();
    }
    
    
    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function getRemind() {
        return View::make('users.forgetPassword');
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind() {
        switch ($response = Password::remind(Input::only('email'), function($message) {
            $message->subject('Password Reminder - WAVINGshop');
        })) {
            case Password::INVALID_USER:
                return Redirect::back()->with('error', Lang::get($response));

            case Password::REMINDER_SENT:
                return Redirect::back()->with('status', Lang::get($response));
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return Response
     */
    public function getReset($token = null) {
        if (is_null($token))
            App::abort(404);

        return View::make('users.resetPassword')->with('token', $token);
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset() {
        $credentials = Input::only(
                        'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function($user, $password) {
                    $user->password = Hash::make($password);

                    $user->save();
                    
                    Auth::login($user);
                });

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()->with('error', Lang::get($response))->withInput();

            case Password::PASSWORD_RESET:
                return Redirect::route('account');
        }
    }

}
