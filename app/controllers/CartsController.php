<?php

class CartsController extends BaseController {
    
    function __construct() {
        parent::__construct();
    }

    /**
     * Shopping car page
     * @return view
     */
    public function shoppingCart() {
        return View::make('carts.shoppingCart');
    }

    /**
     * Checkout page
     * @return view
     */
    public function checkout() {
        return View::make('carts.checkout');
    }

}
