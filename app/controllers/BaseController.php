<?php

class BaseController extends Controller {

    public function __construct() {
        $current = Route::currentRouteName();
        if (in_array($current, ['news', 'newsCategory', 'newsShow'])) {
            if($current == 'news'):
                $news_categories = PostCategory::with(['posts' => function($q){
                    $q->where('status', 1)->orderBy('viewed','desc')->select(['id', 'created_at', 'show_id', 'title', 'description', 'post_category_id', 'image']);
                }])->where(['status' => 1])->select(['id', 'show_id', 'name'])->get();
            else:
                $news_categories = PostCategory::with(['posts' => function($q){
                    $q->where('status', 1)->orderBy('viewed','desc')->select(['id', 'post_category_id']);
                }])->where(['status' => 1])->select(['id', 'show_id', 'name'])->get();
            endif;
            $recent_posts = Post::where('status', 1)->orderBy('created_at', 'desc')->take(4)->get();
            View::share('recent_posts', $recent_posts);
        } else {
            $news_categories = PostCategory::where(['status' => 1])->select(['show_id', 'name'])->get();
        }
        $pCategories = Category::where(['status' => 1, 'parent_id' => 0])->get();
        $sales = DB::select('select show_id, name, image, price, promotion, ((price-promotion)*100/price) as saling from products where status = 1 order by saling desc limit 4');
        View::share(['news_categories' => $news_categories, 'sales' => $sales, 'pCategories' => $pCategories]);
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    /**
     * jsonProcessor - Return json after process
     * @param boolean $processor
     * @param mixed $successMsg
     * @param mixed $failMsg
     * @return json
     */
    public function jsonProcessor($processor, $successMsg, $failMsg) {
        if ($processor) {
            return $this->jsonSuccessMsg($successMsg);
        } else {
            return $this->jsonFailMsg($failMsg);
        }
    }

    /**
     * jsonSuccessMsg - Return json with success is true && custom message
     * @param mixed $msg - Custom message
     * @return json - success:true; msg:$msg
     */
    public function jsonSuccessMsg($msg = "Saved.") {
        return Response::json(array(
                    'success' => true,
                    'msg' => $msg
        ));
    }

    /**
     * jsonFailMsg - Return json with success is false && custom message
     * @param mixed $msg - Custom message
     * @return json - success:false; msg:$msg
     */
    public function jsonFailMsg($msg = "Error, please try later.") {
        return Response::json(array(
                    'success' => false,
                    'msg' => $msg
        ));
    }

    /**
     * jsonFailErrors - Return json with success is false && custom errors
     * @param array $msg - Custom message
     * @return json - success:false; errors:$msg
     */
    public function jsonFailErrors($msg = array()) {
        return Response::json(array(
                    'success' => false,
                    'errors' => $msg
        ));
    }

}
