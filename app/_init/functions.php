<?php

//html entities all input
function eInputs($inputArray) {
    foreach ($inputArray as $key => $value) {
        $inputArray[$key] = e($value);
    }
    return $inputArray;
}

/**
 * image - Return file or default source link
 * @param type $name
 * @param array $folders - folder and subfolders if available
 * @param mixed $preDefault
 * @return mixed
 */
function imageSrc($name, $folders, $preDefault = "") {
    $p = "uploads/";
    if (!empty($name)) {
        $usePath = "";
        foreach ($folders as $value) {
            $usePath .= $value . "/";
        }
        $src = $p . $usePath . $name;
    } else {
        $src = $p . $preDefault . "default.jpg";
    }
    return asset($src);
}

/**
 * checkAttr - Return attribute if passes the comparation, else return ""
 * @param mixed $needle - data to check
 * @param array $objects - array contains data to check
 * @param boolean $onlyValue - return only value of attribute of both attribute name & value
 * @param type $name - attribute name
 * @param type $value - attribute value
 * @return string
 */
function checkAttr($needle, $objects, $onlyValue = false, $name = "class", $value = "active") {
    if (in_array($needle, $objects)) {
        if ($onlyValue) {
            return $value;
        } else {
            return $name . '="' . $value . '"';
        }
    } else {
        return "";
    }
}

/**
 * getDB - Return quick data from database
 * @param type $key
 * @param type $returnField
 * @param type $checkField
 * @param type $model
 * @return string
 */
function getDB($key, $returnField = "value", $checkField = "key", $model = "Setting") {
    $info = $model::where($checkField, $key)->first();
    if (count($info)) {
        return $info->$returnField;
    } else {
        return "";
    }
}

/**
 * makeId - Make an unique random alphanumeric id
 * @param type $model
 * @param type $length
 * @return string
 */
function makeId($model = "Product", $length = 11) {
    $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-';
    $charsLength = strlen($chars);
    $showId = '';
    for ($i = 0; $i < $length; $i++) {
        $showId .= $chars[rand(0, $charsLength - 1)];
    }
    if (!in_array($model, ['Product', 'Post', 'User', 'Category', 'PostCategory'])) {
        App::abort(404);
    }
    $check = $model::whereShowId($showId)->count();
    if ($check) {
        return makeId($model, $length);
    }
    return $showId;
}

/**
 * checkId - get real int id from alphanumeric id
 * @param type $showId - alphanumeric id (created by makeId([...]))
 * @param type $length - length of alphanumeric id too check
 * @return id {int} or throw not found exception
 */

/**
 * 
 * @param mixed $showId - Id to check
 * @param mixed $model - Model [Product/Post/User/Category
 * @param array|mixed $with - relation name
 * @param array $withConditions - relation filter [where/orderBy/select](ex: ['where'=>['name','like','*words*']])
 * @param array $where - filter
 * @param array|mixed $select - selected column(s)
 * @param int $length - number of alphanumeric key to check
 * @return object - object data
 */
function getIdData($showId, $model = "Product", $with = false, $where = false, $select = "*", $length = 11) {
    if (strlen($showId) != $length || !in_array($model, ['Product', 'Post', 'User', 'Category', 'PostCategory'])) {
        App::abort(404);
    } else {
        if ($with && $where) {
            $data = $model::with($with)->where('show_id', $showId)->where($where)->select($select);
        } elseif ($with && !$where) {
            $data = $model::with($with)->where('show_id', $showId)->select($select);
        } elseif (!$with && $where) {
            $data = $model::where('show_id', $showId)->where($where)->select($select);
        } else {
            $data = $model::where('show_id', $showId)->select('id');
        }
        $data = $data->first();
        if ($data) {
            return $data;
        } else {
            App::abort(404);
        }
    }
}
