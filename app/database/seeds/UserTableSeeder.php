<?php

class UserTableSeeder extends Seeder {

    public function run() {
        DB::table('users')->truncate();
        for ($i = 1; $i <= 5; $i++) {
            User::create(array(
                'show_id' => makeId('User'),
                'email' => 'us' . $i . '@waving.pr',
                'password' => Hash::make('123456'),
                'firstname' => 'User ' . $i,
                'lastname' => 'ID' . $i,
                'role_id' => rand(1, 3),
                'fixed_header' => rand(0, 1),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ));
        }
    }

}

class CategoryTableSeeder extends Seeder {

    public function run() {
        DB::table('categories')->truncate();
        for ($i = 1; $i <= 4; $i++) {
            $fake = Faker\Factory::create();
            Category::create(array(
                'show_id' => makeId('Category'),
                'name' => $fake->name,
                'description' => $fake->text(200),
                'user_id' => rand(1, 5),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ));
        }
    }

}

class ProductTableSeeder extends Seeder {

    public function run() {
        DB::table('products')->truncate();
        //$faker = Faker\Factory::create();
        $sizes = ["XS", "S", "M", "L", "XL"];
        $colors = ["red", "green", "blue", "white", "black"];
        $branches = ["Vietnam", "Thailand", "China", "Korea", "Japan"];
        for ($i = 1; $i <= 20; $i++) {
            $price = rand(10.0, 100.0);
            $fake = Faker\Factory::create();
            Product::create(array(
                'show_id' => makeId(),
                'name' => $fake->name,
                'image' => 'model' . rand(1, 7) . ".jpg",
                'image_thumb' => 'k' . rand(1, 4) . ".jpg",
                'description' => $fake->text(200),
                'details' => $fake->text(2000),
                'price' => $price,
                'promotion' => rand($price/2, $price),
                'size' => $sizes[rand(0, 4)],
                'color' => $colors[rand(0, 4)],
                'branch' => $branches[rand(0, 4)],
                'available' => 1,
                'hot' => rand(0, 1),
                'available' => 1,
                'status' => 1,
                'category_id' => rand(1, 4),
                'user_id' => rand(1, 5),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ));
        }
    }

}

class SettingTableSeeder extends Seeder {

    public function run() {
        DB::table('settings')->truncate();
        $keys = ["title", "slogan", "description", "keywords", "author", "email", "phone", "theme_color", "layout", "google_maps", "stmp_email", "stmp_password"];
        for ($i = 0; $i < count($keys); $i++) {
            $fake = Faker\Factory::create();
            Setting::create(array(
                'key' => $keys[$i],
                'value' => $fake->name,
                'description' => $fake->text(200),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ));
        }
    }

}


class PostCategoryTableSeeder extends Seeder {

    public function run() {
        DB::table('post_categories')->truncate();
        for ($i = 1; $i <= 5; $i++) {
            $fake = Faker\Factory::create();
            PostCategory::create(array(
                'show_id' => makeId('PostCategory'),
                'name' => $fake->name,
                'description' => $fake->text(200),
                'user_id' => rand(1, 5),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ));
        }
    }

}

class PostTableSeeder extends Seeder {

    public function run() {
        DB::table('posts')->truncate();
        for ($i = 1; $i <= 20; $i++) {
            $fake = Faker\Factory::create();
            Post::create(array(
                'show_id' => makeId('Post'),
                'title' => $fake->name,
                'image' => 'img' . rand(1, 6) . ".jpg",
                'image_thumb' => 'k' . rand(1, 4) . ".jpg",
                'description' => $fake->text(200),
                'details' => $fake->text(2000),
                'hot' => rand(0, 1),
                'status' => 1,
                'post_category_id' => rand(1, 5),
                'user_id' => rand(1, 5),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ));
        }
    }

}