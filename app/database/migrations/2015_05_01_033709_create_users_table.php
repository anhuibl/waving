<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('show_id', 20)->unique()->nullable();
            $table->string('email', 100);
            $table->string('password');
            $table->rememberToken();
            $table->string('firstname', 50);
            $table->string('lastname', 50);
            $table->string('phone', 20)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('avatar')->nullable();
            $table->string('theme_color', 20)->nullable();
            $table->smallInteger('fixed_header')->default(1);
            $table->smallInteger('status')->default(1);
            $table->smallInteger('role_id')->default(3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }

}
