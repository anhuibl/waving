<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('show_id', 20)->unique()->nullable();
            $table->string('name');
            $table->string('image');
            $table->string('image_thumb');
            $table->text('description');
            $table->text('details');
            $table->float('price');
            $table->float('promotion')->nullable();
            $table->string('size')->nullable();
            $table->string('color')->nullable();
            $table->string('branch')->nullable();
            $table->bigInteger('viewed')->default(0);
            $table->tinyInteger('available')->default(0);
            $table->tinyInteger('hot')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->string('meta_title')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->integer('category_id');
            $table->bigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('products');
    }

}
