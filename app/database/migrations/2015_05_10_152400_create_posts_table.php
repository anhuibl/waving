<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('posts', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('show_id', 20)->unique()->nullable();
            $table->string('title');
            $table->string('image');
            $table->string('image_thumb');
            $table->text('description');
            $table->text('details');
            $table->bigInteger('viewed')->default(0);
            $table->tinyInteger('hot')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->string('meta_title')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->integer('post_category_id');
            $table->bigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('posts');
    }

}
